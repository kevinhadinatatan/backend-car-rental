const express = require('express');
const usersController = require('../controllers/api/UsersController');
const AuthController = require('../controllers/api/AuthController');
// const UserModel = require('../models/UsersModel');

function routes() {
    const router = express.Router();
    // const usersController = UsersController();

    router.get('/user', function (req, res) {
        usersController.index(req, res);
    });

    router.post('/user', function (req, res) {
        usersController.create(req, res);
    });

    router.put('/user/:userID', function (req, res) {
        usersController.update(req, res);
    });

    router.delete('/user/:userID', function (req, res) {
        usersController.remove(req, res);
    });

    router.get('/user/:userID', function (req, res) {
        usersController.getByUserID(req, res);
    });

    router.post('/auth/login', function (req, res) {
        AuthController.login(req, res);
    });

    return router;
}

module.exports = routes;
